angular.module('inventory', ['ui.bootstrap', 'ui.utils', 'ui.router']);

angular.module('inventory').config(function($stateProvider) {

    $stateProvider.state('inventoryAssignments', {
        url: '/inventory',
        parent: 'root',
        templateUrl: 'inventory/partial/inventory.html',
        controller: 'InventoryCtrl',
        redirectTo: 'inventoryAssignments.list'
    })

    .state('inventoryAssignments.list', {
            url: '/list',
            templateUrl: 'inventory/partial/inventory-list.html'
    })

    .state('inventoryAssignments.add', {
            url: '/add',
            templateUrl: 'inventory/partial/add-inventory.html',
            controller: 'InventoryCtrl'

    })
    .state('inventoryAssignments.edit', {
            url: '/edit',
            templateUrl: 'inventory/partial/edit-inventory.html'
    })

    .state('inventoryAssignments.tree', {
        url: '/inventorytree',
        templateUrl: 'inventory/partial/inventoryStructure.html'
    })

    .state('inventoryAssignments.child', {
        url: '/child',
        templateUrl: 'inventory/partial/inventorychild.html'
    })

});
