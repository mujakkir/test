angular.module('inventory').controller('InventoryCtrl', function($scope,User, $stateParams, $location, InventoryMasterService, InventoryService) {

    var inventoryTree = null;
    $scope.empId = $stateParams.empId;
//    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.inventories={};

    $scope.inventoryAssignments = {};


    function getInventory() {

    InventoryService.getInventory.view({}, function(data) {
        console.log('data :', typeof data, data);
        $scope.InventoryAssignInfo = data;
    });
}

    getInventory();


    $scope.addinventoryAssignments = function() {
        $scope.inventoryAssignments = {};
        $location.path('/inventory/add');
    }

    $scope.editInventoryAssignments = function(inven) {
        $scope.inventoryAssignments=angular.extend({},inven);
        console.log(inven);
        $location.path('/inventoryAssignments/edit');
    }


   /* InventoryMasterService.getInventory.view({}, function(data) {
        console.log('data :', typeof data, data);
        $scope.InventoryInfo = data;
    });*/


    $scope.delete = function() {

        InventoryService.deleteInventory.delete($scope.currentInventory, function(data) {
            for (var i = 0; i < $scope.InventoryAssignInfo.length; i++) {
                if ($scope.InventoryAssignInfo[i].entryTime == $scope.currentInventory.entryTime) {
                    $scope.InventoryAssignInfo.splice(i, 1);
                }
            }
            $scope.currentInventory = null;
            $('#confirmDelete').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        });
     }

    $scope.setCurrentInventory = function(InventoryAssignments) {        controller: 'InventoryCtrl'

        $scope.currentInventory = InventoryAssignments;
    }


    $scope.save = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            
            console.log($scope.currentItemType.itemType);
            $scope.inventoryAssignments.itemType = $scope.currentItemType.itemType;
            $scope.inventoryAssignments.category = $scope.currentCategory.category;

            InventoryService.addInventory.add($scope.inventoryAssignments, function(data) {
                if (data.result === 'added') {
                    $scope.inventoryAssignments ={};
                    getInventory();
                    $location.path('/inventoryAssignments/list')
                }   

            });
        }
        else {
             angular.forEach(form.$error.required, function(field) {
            field.$setDirty();
            });
        }
    }


        $scope.update = function(event, form) { 
            console.log("in update ofinventoryAssignments");
            event.preventDefault();

            if (form.$valid) {
                     console.log($scope.inventoryAssignments);
            console.log($scope.currentItemType);
            $scope.inventoryAssignments.itemType = $scope.currentItemType.itemType;
            $scope.inventoryAssignments.category = $scope.currentCategory.category;
             console.log($scope.inventoryAssignments);
            InventoryService.editInventory.edit($scope.inven, function(data) {
                 console.log($scope.inventoryAssignments);
                 console.log($scope.inventoryAssignments);
                $scope.inventoryAssignments = data;
                console.log($scope.inventoryAssignments);
                getInventory();
                $location.path('/inventoryAssignments/list');
            
            });
        }
        else {
             angular.forEach(form.$error.required, function(field) {
            field.$setDirty();
        });
        }
    }

    
    function loadChildren() {
        inventoryTree.get({}, function(data) {
            $scope.currentEmp = data.result.selfProfile;
            //$scope.people = data.result.childList;
            $scope.people = data.result.invtryChildList;
            $scope.inventories = data.result.invtryChildList;
            console.log( $scope.inventories);
        });
    }

    if ($stateParams.empId) {
        inventoryTree = InventoryService.inventoryTree($stateParams.empId);
        loadChildren();
    }
    else {
        inventoryTree = InventoryService.inventoryTree(User.id);
        loadChildren();
    }

    $scope.showInventoryTree = function(empId) {
        console.log(empId);
        $location.path('/inventorytree/' + empId);
    }   

});

/*angular.module('inventory').run(['$rootScope', '$state', function($rootScope, $state){
      $rootScope.$on('$stateChangeStart', function(event, toState) {
        var redirect = toState.redirectTo;
        if (redirect) {
          event.preventDefault();
          if(angular.isFunction(redirect))
              redirect.call($state);
          else
              $state.go(redirect);
        }
      });
    }]);*/