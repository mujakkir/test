angular.module('inventory').service('InventoryService',function($resource, $cookies) {

console.log('ser called');

    this.getInventory = $resource('/gsmart/inventoryAssignments/view', {}, {
        view: { method: 'POST', isArray: true }
    });

    this.addInventory = $resource('/gsmart/inventoryAssignments/add/'+$cookies.get('userId'), {}, {
        add: { method: 'POST' }
    });

    this.editInventory = $resource('/gsmart/inventoryAssignments/edit/'+$cookies.get('userId'), {}, {

        edit: { method: 'POST' }
    });

    this.deleteInventory = $resource('/gsmart/inventoryAssignments/delete/48750001', {}, {
        delete: { method: 'POST' }
    });

    this.inventoryTree = function (id) {
        return $resource('/gsmart/inventorySearch/info/' + id, {}, {
            get: { method: 'POST', headers: { 'Content-Type': 'application/json' } }
        })
    }
});
	
