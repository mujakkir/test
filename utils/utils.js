angular.module('utils', []).factory('utils', function() {
	return {
		getDateString: function (timestamp) {
			var date = new Date(timestamp);
			return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
		}
	}
})