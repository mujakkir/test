/*jslint node: true */
'use strict';

var pkg = require('./package.json');
var path = require('path');

//Using exclusion patterns slows down Grunt significantly
//instead of creating a set of patterns like '**/*.js' and '!**/node_modules/**'
//this method is used to create a set of inclusive patterns for all subdirectories
//skipping node_modules, bower_components, dist, and any .dirs
//This enables users to create any directory structure they desire.
var createFolderGlobs = function(fileTypePatterns) {
  fileTypePatterns = Array.isArray(fileTypePatterns) ? fileTypePatterns : [fileTypePatterns];
  var ignore = ['node_modules','bower_components','dist','.tmp'];
  var fs = require('fs');
  return fs.readdirSync(process.cwd())
          .map(function(file){
            if (ignore.indexOf(file) !== -1 ||
                file.indexOf('.') === 0 ||
                !fs.lstatSync(file).isDirectory()) {
              return null;
            } else {
              return fileTypePatterns.map(function(pattern) {
                return file + '/**/' + pattern;
              });
            }
          })
          .filter(function(patterns){
            return patterns;
          })
          .concat(fileTypePatterns);
};

var lessCreateConfig = function (context, block) {
    console.log('creating less config');
    var cfg = {files: []};
    var outfile = path.join(context.outDir, block.dest);
    var filesDef = {};
     
    filesDef.dest = outfile;
    filesDef.src = [];
         
    context.inFiles.forEach(function (inFile) {
        filesDef.src.push(path.join(context.inDir, inFile));
    });
    console.log(cfg);
    cfg.files.push(filesDef);
    context.outFiles = [block.dest];
    console.log(cfg);
    return cfg;
};

var ngAnnotateCreateConfig = function (context, block) {
  var cfg = {
    files: []
  };

  if (block.dest === 'app.js') {
    block.src.forEach(function (src) {
      //cfg.main.files['.tmp/annotate/' + src] = [src];
      cfg.files.push({
        'src': [src],
        'dest': '.tmp/annotate/' + src
      })
    });
  }

  cfg.files.push({
    'src': ['.tmp/templates.js'],
    'dest': '.tmp/annotate/templates.js'
  });

  return cfg;
}

module.exports = function (grunt) {

  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    connect: {
      main: {
        options: {
          port: 9001
        }
      }
    },
    watch: {
      main: {
        options: {
            livereload: true,
            livereloadOnError: false,
            spawn: false
        },
        files: [createFolderGlobs(['*.js','*.less','*.html']),'!_SpecRunner.html','!.grunt'],
        tasks: [] //all the tasks are run dynamically during the watch event handler
      }
    },
    jshint: {
      main: {
        options: {
            jshintrc: '.jshintrc'
        },
        src: createFolderGlobs('*.js')
      }
    },
    clean: {
      before:{
        src:['dist','.tmp']
      },
      after: {
        src:['.tmp']
      }
    },
    htmlmin: {
      main: {
        options: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        },
        files: {
          'dist/index.html': 'dist/index.html'
        }
      }
    },
    ngtemplates: {
      main: {
        options: {
            module: pkg.name,
            /*htmlmin: '<%= htmlmin.main.options %>',*/
            htmlmin: {}
        },
        src: [createFolderGlobs(['*.html']), '!index.html', '!_SpecRunner.html'],
        dest: '.tmp/templates.js'
      }
    },
    copy: {
      assets: {
        files: [
          {src: ['img/**'], dest: 'dist/'},
          {src: ['bower_components/**'], dest: 'dist/'}
        ]
      },
      generated: {
        src: 'index.html',
        dest: 'dist/index.html'
      }
    },
    filerev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        length: 20
      },
      source: {
        files: [{
          src: [
            'dist/js/*.js',
            'dist/css/*.css'
          ]
        }]
      }
    },
    useminPrepare: {
      html: 'index.html',
      options: {
        dest: 'dist',
        flow: {
          steps: {
            'js': [{
              name: 'ngAnnotate',
              createConfig: ngAnnotateCreateConfig
            }, 'concat', 'uglifyjs'],
            'css': ['concat', 'cssmin'],
            'less': [{
              name: 'less',
              createConfig: lessCreateConfig
            }]
          },
          post: {
            js: [{
              name: 'uglifyjs',
              createConfig: function (context, block) {
                var generated = context.options.generated;
                console.log(generated);
                generated.files.forEach(function (file) {
                  console.log(file);
                  if(file.dest === 'dist/app.js') {
                    file.src.push('.tmp/concat/templates.js');
                  }
                })
              }
            }, {
              name: 'concat',
              createConfig: function (context, block) {
                var generated = context.options.generated;

                if (block.dest === 'app.js') {
                  generated.files.forEach(function (file) {
                   if(file.dest === '.tmp/concat/app.js') {
                      block.src.forEach(function(src) {
                          file.src.push('.tmp/annotate/' + src);
                      });
                      file.src.push('.tmp/annotate/templates.js');   
                   }
                  });
                }
                else if (block.dest === 'vendor.js') {
                  generated.files.forEach(function (file) {
                   if(file.dest === '.tmp/concat/vendor.js') {
                      block.src.forEach(function(src) {
                          file.src.push(src);
                      })     
                   }
                  });
                }
                

                console.log('concat conf :', generated);
              }
            }]
          }
        }
      }
    },
    usemin: {
      html: 'dist/index.html',
      options: {
        assetsDirs: ['dist', 'dist/css', 'dist/js'],
        blockReplacements: {
          less: function (block) {
              return '<link rel="stylesheet" href="' + block.dest + '" />';
          }
        }
      }
    },
    dom_munger:{
      read: {
        options: {
          read:[
            {selector:'script[data-concat!="false"]',attribute:'src',writeto:'appjs'},
            {selector:'link[rel="stylesheet"][data-concat!="false"]',attribute:'href',writeto:'appcss'}
          ]
        },
        src: 'index.html'
      },
      update: {
        options: {
          remove: ['script[data-remove!="false"]','link[data-remove!="false"]'],
          append: [
            {selector:'body',html:'<script src="app.full.min.js"></script>'},
            {selector:'head',html:'<link rel="stylesheet" href="app.full.min.css">'}
          ]
        },
        src:'index.html',
        dest: 'dist/index.html'
      }
    },
    /*cssmin: {
      main: {
        src:['<%= dom_munger.data.appcss %>'],
        dest:'dist/app.full.min.css'
      }
    },
    concat: {
      main: {
        src: ['<%= dom_munger.data.appjs %>','<%= ngtemplates.main.dest %>'],
        dest: '.tmp/app.full.js'
      }
    },*/
    /*ngAnnotate: {
      main: {
        src:'dist/app.js',
        dest: 'dist/app.js'
      }
    },*/
    /*uglify: {
      main: {
        src: '.tmp/app.full.js',
        dest:'dist/app.full.min.js'
      }
    },*/
    //Imagemin has issues on Windows.  
    //To enable imagemin:
    // - "npm install grunt-contrib-imagemin"
    // - Comment in this section
    // - Add the "imagemin" task after the "htmlmin" task in the build task alias
    // imagemin: {
    //   main:{
    //     files: [{
    //       expand: true, cwd:'dist/',
    //       src:['**/{*.png,*.jpg}'],
    //       dest: 'dist/'
    //     }]
    //   }
    // },
    /*karma: {
      options: {
        frameworks: ['jasmine'],
        files: [  //this files data is also updated in the watch handler, if updated change there too
          '<%= dom_munger.data.appjs %>',
          'bower_components/angular-mocks/angular-mocks.js',
          createFolderGlobs('*-spec.js')
        ],
        logLevel:'ERROR',
        reporters:['mocha'],
        autoWatch: false, //watching is handled by grunt-contrib-watch
        singleRun: true
      },
      all_tests: {
        browsers: ['PhantomJS','Chrome','Firefox']
      },
      during_watch: {
        browsers: ['PhantomJS']
      },
    }*/
  });

  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-filerev');
  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask('build',[
    'clean:before',
    'copy:generated', 
    'useminPrepare',
    'ngtemplates',
    'ngAnnotate', 
    'concat', 
    'less',
    'uglify',
    'filerev',
    'usemin',
    'copy:assets'/*,
    'clean:after'*/
  ]);
    //'dom_munger','ngtemplates','cssmin','concat', 'uglify','copy','htmlmin','clean:after']);
  grunt.registerTask('serve', ['dom_munger:read','jshint','connect', 'watch']);
  grunt.registerTask('test',['dom_munger:read','karma:all_tests']);

  grunt.event.on('watch', function(action, filepath) {
    //https://github.com/gruntjs/grunt-contrib-watch/issues/156
    var tasksToRun = [];

    if (filepath.lastIndexOf('.js') !== -1 && filepath.lastIndexOf('.js') === filepath.length - 3) {

      //lint the changed js file
      grunt.config('jshint.main.src', filepath);
      tasksToRun.push('jshint');

      //find the appropriate unit test for the changed file
      var spec = filepath;
      if (filepath.lastIndexOf('-spec.js') === -1 || filepath.lastIndexOf('-spec.js') !== filepath.length - 8) {
        spec = filepath.substring(0,filepath.length - 3) + '-spec.js';
      }

      //if the spec exists then lets run it
      if (grunt.file.exists(spec)) {
        var files = [].concat(grunt.config('dom_munger.data.appjs'));
        files.push('bower_components/angular-mocks/angular-mocks.js');
        files.push(spec);
        grunt.config('karma.options.files', files);
        tasksToRun.push('karma:during_watch');
      }
    }

    //if index.html changed, we need to reread the <script> tags so our next run of karma
    //will have the correct environment
    if (filepath === 'index.html') {
      tasksToRun.push('dom_munger:read');
    }

    grunt.config('watch.main.tasks',tasksToRun);

  });
};
