angular.module('maintenance').service('NoticeService', function($resource, config, $q) {

    var organizationNotices = null;


    var notice = $resource(config.baseUrl + '/notice/:smartId/:ts/', { smartId: '@smartId', ts: '@timeStamp' }, {
        'update': { method: 'PUT' }
    });

    this.get = function(smartId) {
        return notice.query({ smartId: smartId }).$promise;
    }

    this.save = function(data) {
        return notice.save(data).$promise;
    }

    this.update = function(data) {
        return notice.update({ ts: null }, data).$promise;
    }

    this.delete = function(timestamp) {
        return notice.delete({ ts: timestamp.toString() }).$promise;
    }



    var orgnotice = $resource(config.baseUrl + '/notice/childNotice/:smartId/', { smartId: '@smartId' }, {
        'update': { method: 'PUT' }
    });
    /*
        this.get1= function (smartId) {
            return orgnotice.get({smartId:smartId}).$promise;
        }*/




    this.get1 = function(smartId) {
        var defer = $q.defer();
        var params = {
            smartId: smartId
        };

        orgnotice.get(params).$promise.then(function(data) {
            organizationNotices = data;
            defer.resolve(data);
        }, function(err) {
            defer.reject(err);
        });

        return defer.promise;
    }


    this.childNoticesDetails = function(id, callback) {

       
        var childNoticesDetail = [];
        console.log(organizationNotices);

        if (organizationNotices && organizationNotices.childNotices.length > 0) {

            for (var i = 0; i < organizationNotices.childNotices.length >0; i++) {

            console.log( 'data' +  typeof organizationNotices.childNotices[i].smart_id, organizationNotices.childNotices[i].smart_id  );
              
            console.log(organizationNotices.childNotices[i].smart_id == id.toString());

                if (organizationNotices.childNotices[i].smart_id == id.toString()) {

                    console.log(id);

                    childNoticesDetail.push(organizationNotices.childNotices[i]);

                    console.log(childNoticesDetail);

                   // break;
                }
            }
            callback(childNoticesDetail);
        } else {
            //make the web service call to fetch employee details using id
        }
    }



    /*
        Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    // Get the size of an object
    var size = Object.size(childNotices);*/

});
