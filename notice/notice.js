angular.module('notice', ['ui.bootstrap', 'ui.utils', 'ui.router']);

angular.module('notice').config(function($stateProvider) {


    $stateProvider.state('notice', {
            url: '/notice',
            parent: 'app',
            templateUrl: 'notice/partial/home.html',
            controller: 'NoticeCtrl',
            resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        var template = null;
                        if (user.profile.role.toLowerCase() === 'teacher') {
                            
                            template = 'notice/partial/notice.html';
                        } else {
                            
                            template = 'notice/partial/decider.html';
                        }
                        defer.resolve(template);
                    });
                    return defer.promise;
                }   
            }
        })
        .state('notice.list', {
            url: '/list',
            templateUrl: 'notice/partial/notice.html'
        })
        .state('notice.add', {
            url: '/add',
            templateUrl: 'notice/partial/add-notice.html'
        })
        .state('notice.edit', {
            url: '/edit',
            templateUrl: 'notice/partial/edit-notice.html'
        })
        .state('notice.org', {
            url: '/org',
            templateUrl: 'notice/partial/notice-orgstructure.html'
        })
        .state('notice.org2', {
            url: '/:empId',
            templateUrl: 'notice/partial/notice-orgstructure.html'
        })
        .state('notice.child', {
            url: '/child/:empId',
            templateUrl: 'notice/partial/child-notice.html',
            controller:'NoticeOrgCtrl'
        })
     
});
