angular.module('notice').controller('NoticeCtrl', function($scope, User, $stateParams, NoticeService, $location, $cookies, template) {
    
    $scope.template=template;
    var noticeTree = null;
    $scope.empId = $stateParams.empId;
    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.notices = {};



    $scope.Notice = {};

    function getNotices() {
        console.log(User.id);
        NoticeService.get(User.id).then(function(data) {
            $scope.NoticeInfo = data;
            console.log(data);
        }, function(err) {
            console.log(err);
        });
    }

    getNotices();

    $scope.addNotice = function() {
        console.log('addNotice called')
        $scope.Notice = {};
        $scope.addNoticeFormError = null;
        $location.path('/notice/add');
    }

    $scope.editNotice = function(Notice) {
        $scope.Notice = angular.extend({}, Notice);
        $location.path('/notice/edit');
    }

    $scope.savenotice = function(event, form) {
        event.preventDefault();
        $scope.Notice.smart_id = $cookies.get('userId')

        if (form.$valid) {
            NoticeService.save($scope.Notice).then(function(data) {
                $scope.Notice = {};
                getNotices();
                $location.path('/notice')
            }, function(err) {
                $scope.addNoticeFormError = err.data.message;
                console.log('Error while adding Notice :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }


    $scope.update = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            console.log($scope.Notice);
            NoticeService.update($scope.Notice).then(function(data) {
                $scope.Notice = {};
                getNotices();
                $location.path('/notice')
            }, function(err) {
                $scope.editNoticeFormError = err.data.message;
                console.log('Error while adding Notice :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {
        
        NoticeService.delete($scope.currentNotice.entryTime).then(function(data) {
            for (var i = 0; i < $scope.NoticeInfo.length; i++) {
                if ($scope.NoticeInfo[i].entryTime == $scope.currentNotice.entryTime) {
                    $scope.NoticeInfo.splice(i, 1);
                }
            }
            $scope.currentNotice = null;
            $('#confirmDelete').modal('hide');
        }, function(err) {
            console.log('Error while deleting record :', err);
        })
    }

    $scope.setCurrentNotice = function(Notice) {
        $scope.currentNotice = Notice;
    }

});
