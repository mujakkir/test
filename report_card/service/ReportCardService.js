angular.module('reportCard').service('fileUpload', ['$resource', function($resource, User) {


    var fileUploadurl = $resource( 'http://localhost:8080/gsmart1/excelToDB/48750001', {}, {
        'update': { method: 'PUT' }
    });


    this.uploadFile = function(data) {
        return fileUploadurl.save(data).$promise;
    }

}]);







/*


    this.uploadFileToUrl = function(file, uploadUrl) {
        var fd = new FormData();
        fd.append('file', file);

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': multipart/form-data }
        })

        .success(function() {})

        .error(function() {});
    }
*/