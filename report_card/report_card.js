angular.module('reportCard', ['ui.bootstrap','ui.utils','ui.router']);

angular.module('reportCard').config(function($stateProvider) {

    $stateProvider.state('report-card', {
        url: '/reportcard',
        parent:'app',
        templateUrl: 'report_card/partial/report-card.html',
        controller: 'ReportCardCtrl'
    });
    /* Add New States Above */

});

