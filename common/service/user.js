angular.module('common').service('User',function($resource, $cookies, $rootScope, config, $q, $window) {
	this.userInfo = null;
	this.identified = false;
	this.id = null;
	var context = this;
	
	var authAPI = $resource(config.baseUrl + '/login/auth', {}, {
		authUser : {
			method : 'POST'
		}
	});

	this.authenticate = function (id, password, callback) {
		console.log("calling authenticate function");
		authAPI.authUser({smartId: id, password: password}, function (response) {
			if (response.data) {
				context.id = id;
				$cookies.put('userId', id);
				$cookies.put('password', password);
				context.userInfo = response.data;
				console.log(context.userInfo)
				$rootScope.role = context.userInfo.profile.role;
				$cookies.put('role', context.userInfo.profile.role);
				console.log($rootScope.role);
				callback(null, response);
			} else {
				callback(response)
			}
		});
	}
	
	this.getUserInfo = function (callback) {
		if (this.userInfo) {
			console.log(this.userInfo);
			//console.log($window.localStorage.getItem('permission'));
			callback(this.userInfo);
		}
		else {
			if ($cookies.get('userId') && $cookies.get('password')) {
				this.id = $cookies.get('userId');
				this.authenticate($cookies.get('userId'), $cookies.get('password'), function (err, data) {
				    console.log(data);
				    context.userInfo = data.data;
					callback(data.data);
				});
			}
			else {
				callback(null);
			}
		}
	};

	this.getUser = function () {
		var defer = $q.defer();
		if (this.userInfo) {
			defer.resolve(this.userInfo);
		}
		else {
			if ($cookies.get('userId') && $cookies.get('password')) {
				this.id = $cookies.get('userId');
				this.authenticate($cookies.get('userId'), $cookies.get('password'), function (err, data) {
				    console.log(data);
				    context.userInfo = data.data;
					defer.resolve(data);
				});
			}
			else {
				defer.resolve(null);
			}
		}
		return defer.promise;	
	}
	
	this.unidentify = function () {
		$cookies.remove('userId');
		$cookies.remove('password');
		this.userInfo = null;
		this.id = null;
		$rootScope.$broadcast('loginChanged');
	};
});