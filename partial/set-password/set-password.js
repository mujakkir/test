angular.module('gsmart').controller('SetPasswordCtrl', function($scope, $stateParams, SetPasswordService) {
    
    $scope.form = {};

    $scope.setPassword = function(event, form) {
        if ($scope.setPasswordForm.$valid) {
            $scope.form.smartId = $stateParams.smartId;
            console.log($scope.form);
            SetPasswordService.save($scope.form).then(function() {
                console.log('success');
                $scope.success = true;
            }, function(err) {
                console.log(err);
                $scope.error = err;
            });
        } else {
            console.log('form not valid');
        }
    }

});
