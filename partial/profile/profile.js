angular.module('gsmart').controller('ProfileCtrl', function($scope,$location, User,template,registration) {

    console.log('profileCtrl called');
    console.log(template);
    console.log(User);
    $scope.template=template;
    $scope.profile = User.userInfo.profile;
    $scope.profile.dob = new Date($scope.profile.dob);
    $scope.profile.joiningDate = new Date($scope.profile.joiningDate);
    $scope.profile.passportIssueDate = new Date($scope.profile.passportIssueDate);
    $scope.profile.passportExpiryDate = new Date($scope.profile.passportExpiryDate);
    console.log($scope.profile);

	$scope.update = function(event, registrationForm) {
        if (registrationForm.$valid) {
			registration.editProfile.update($scope.profile, function(data) {
                $scope.profiles = data;
                $location.path('/registration')
            });
        } else {
            console.log('editProfile err'+registrationForm);
        }
    }
});
