angular.module('gsmart').controller('LoginCtrl', function($scope, $location, User) {
   
    $scope.credentials = {};

    $scope.authenticateUser = function() {

        User.authenticate($scope.credentials.smartId, $scope.credentials.password, function(err, data) {
            console.log(err);
            console.log(!err);
            if (!err) {
                $location.path('/home');
            } else {
                $scope.loginError = err;
            }
        });
    }
});
