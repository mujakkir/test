angular.module('gsmart').controller('HomeCtrl', function($scope, $rootScope, User, $location) {
    $scope.currentPage = $location.path().split('/')[1];
    
    User.getUserInfo(function(userInfo) {
        $scope.modules = userInfo.permissions;
        $scope.details=userInfo.profile;
    });

    $scope.logout = function() {
    	console.log('logout called')
        User.unidentify();
    }

    $rootScope.$on('$stateChangeSuccess', function() {
        $scope.currentPage = $location.path().split('/')[1];
    });
});
