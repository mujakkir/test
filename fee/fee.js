angular.module('fee', ['ui.bootstrap','ui.utils','ui.router']);

angular.module('fee').config(function($stateProvider) {

    $stateProvider.state('fee', {
        url: '/fee',
        parent:'app',
        templateUrl: 'fee/partial/home.html',
        controller: 'FeeCtrl',
         resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        var template = null;
                        if (user.profile.role.toLowerCase() === 'student') {
                            
                            template = 'fee/partial/fee.html';
                        } 
                        else if(user.profile.role.toLowerCase() === 'teacher') {
                            
                            template = 'fee/partial/feeOrgStructure.html';
                        }
                        else
                        {
                            template = 'fee/partial/decider.html';
                        }


                        defer.resolve(template);
                    });
                    return defer.promise;
                }   
            }
         })
    
        .state('fee.add', {
            url: '/add',
            templateUrl: 'fee/partial/add-feestructure.html'
        })

        .state('fee.org', {
            url: '/org',
            templateUrl: 'fee/partial/feeOrgStructure.html'
        })
        .state('fee.employee', {
            url: '/:empId',
            templateUrl: 'fee/partial/feeOrgStructure.html'
        })

});