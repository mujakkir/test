angular.module('fee').service('FeeService', function($resource, config,  $q) {

    var Fee = $resource(config.baseUrl + '/fee/viewFee', {
        'update': { method: 'PUT' }
    });

    this.save = function(data) {
        return Fee.save(data).$promise;
    }


    var Fee1 = $resource(config.baseUrl + '/fee', {});

    this.savefees = function(data) {
        return Fee1.save(data).$promise;
    }


   
    var Fee2 = $resource(config.baseUrl + '/fee/:smartId', {smartId: '@smartId'}, {
        'update': { method: 'PUT' }
    });


    this.getOrg = function(smartId) {
        var defer = $q.defer();
        var params = {
            smartId: smartId
        };

        Fee2.get(params).$promise.then(function(data) {
            console.log(data);
            defer.resolve(data);
        }, function(err) {
            defer.reject(err);
        });

        return defer.promise;
    }

   


});
