angular.module('fee').controller('FeeOrgCtrl', function($scope, User, $stateParams, FeeService, $location) {

    $scope.childNotices = {};

    var parentId = $stateParams.empId ? $stateParams.empId : User.id;

    

    FeeService.getOrg(parentId).then(function(data) {
        console.log(data);
        $scope.currentEmp = data.selfProfile;
          console.log( $scope.currentEmp );
        $scope.people = data.childProfile;
           console.log( $scope.people );
    }, function(err) {
        console.log('Error while fetching FeeOrg :', err);
    });
    


    $scope.showOrganizationTree = function(empId) {
        console.log($location.path('/fee/' + empId));
        $location.path('/fee/' + empId);
    }




});
