angular.module('fee').controller('FeeCtrl', function($scope, $cookies, User, $stateParams, FeeMasterService, template, FeeService, $location) {


    $scope.FeeStructure = {};
    $scope.template = template;
    $scope.fee = {};
    $scope.feespayment = {};
    
    $scope.getFees = function() {
        getFee();
    }


    FeeMasterService.get().then(function(data) {
        $scope.FeeInfo = data;
    }, function(err) {
        console.log(err);
    });

    function getFee() {
        // $scope.FeeStructure.smartId = $cookies.get("userId")
        $scope.FeeStructure.academicYear = '2016-2017';
        FeeService.save($scope.FeeStructure).then(function(data) {
            $scope.FeeStructureInfo = data.result;
        }, function(err) {
            console.log(err);
        });

    };

    getFee();




    $scope.addFee = function() {
        $scope.fee = {};
        $scope.addFeeFormError = null;


        $location.path('/fee/add');
    }


    $scope.setFees = function(argument) {


        $scope.feespayment = _.find($scope.FeeInfo, { standard: argument.standard });
        $scope.FeeStructure.sportsFee = $scope.feespayment.sportsFee
        $scope.FeeStructure.tuitionFee = $scope.feespayment.tuitionFee
        $scope.FeeStructure.totalFee = $scope.feespayment.totalFee
        $scope.FeeStructure.miscellaneousFee = $scope.feespayment.miscellaneousFee

    }

    $scope.save = function(event, form) {
        event.preventDefault();
        $scope.FeeStructure.smartId = $cookies.get('userId')
        console.log('save called');
        if (form.$valid) {

            console.log($scope.FeeStructure);
            FeeService.savefees($scope.FeeStructure).then(function(data) {

                $scope.FeeStructure = {};
                alert("Success");
                $scope.getFees();
                $location.path('/fee')
            }, function(err) {
                $scope.addFeeFormError = err.data.message;
                console.log('Error while adding Notice :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }
});
