angular.module('gsmart').directive('navbar', function($state) {
	return {
		restrict: 'A',
		scope: {
			navbarItems : '='
		},
		templateUrl: 'directive/navbar/navbar.html',
		link: function(scope, element, attrs, fn) {
			scope.adjustMenuItems = function (last) {
				if(last) {
					var children = element.children('li');
					var childWidth = 0;
					var parentWidth = element.width();
					for(var i=0; i<children.length; i++) {
						childWidth += $(children[i]).width();
						if (childWidth > 0.8 * parentWidth) {
							scope.childLimit = i;
							console.log('child limit :', scope.childLimit);
							break;
						}
					} 
				}
			}

			$(window).on('resize', function () {
				if (scope.childLimit !== scope.navbarItems.length) {
					scope.childLimit = scope.navbarItems.length;
				}
				else {
					scope.adjustMenuItems(true);
				}
				scope.$apply();
			});
		}
	};
});
