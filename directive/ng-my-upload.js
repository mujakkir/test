 angular.module('gsmart').directive('fileModel', function($parse) {
     return {
         restrict: 'A',
         link: function(scope, element, attrs) {
             var model = $parse(attrs.fileModel);
             console.log(model)
             var modelSetter = model.assign;

             element.bind('change', function() {
                 scope.$apply(function() {
                     modelSetter(scope, element[0].files[0]);
                 });

                 console.log(scope);
             });
         }
     };
 });
