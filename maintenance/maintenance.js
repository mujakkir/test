angular.module('maintenance', ['appConfig', 'ui.bootstrap', 'ui.utils', 'ui.router', 'utils']);

angular.module('maintenance').config(function($stateProvider) {


    $stateProvider.state('maintenance', {
        url: '/maintenance',
        parent: 'app',
        templateUrl: 'maintenance/partial/home.html',
    })

    $stateProvider.state('maintenance.band', {
            url: '/band',
            templateUrl: 'maintenance/partial/band/band.html',
            controller: 'BandCtrl'
        })
        .state('maintenance.band.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/band/add-band.html'
        })
        .state('maintenance.band.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/band/edit-band.html'
        })


    $stateProvider.state('maintenance.hierarchy', {
            url: '/hierarchy',
            templateUrl: 'maintenance/partial/hierarchy/hierarchy.html',
            controller: 'HierarchyCtrl'
        })
        .state('maintenance.hierarchy.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/hierarchy/add-hierarchy.html'
        })
        .state('maintenance.hierarchy.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/hierarchy/edit-hierarchy.html'
        })

    $stateProvider.state('maintenance.holiday', {
            url: '/holiday',
            templateUrl: 'maintenance/partial/holiday/holiday.html',
            controller: 'HolidayCtrl'
        })
        .state('maintenance.holiday.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/holiday/add-holiday.html'
        })
        .state('maintenance.holiday.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/holiday/edit-holiday.html'
        })

    $stateProvider.state('maintenance.permissions', {
            url: '/permissions',
            templateUrl: 'maintenance/partial/permissions/permissions.html',
            controller: 'PermissionsCtrl'
        })
        .state('maintenance.permissions.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/permissions/add-permissions.html'
        })
        .state('maintenance.permissions.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/permissions/edit-permissions.html'
        })

    $stateProvider.state('maintenance.inventory', {
            url: '/inventory',
            templateUrl: 'maintenance/partial/inventory/inventory.html',
            controller: 'InventoryMasterCtrl'
        })
        .state('maintenance.inventory.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/inventory/add-inventory.html'
        })
        .state('maintenance.inventory.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/inventory/edit-inventory.html'
        });

    $stateProvider.state('maintenance.fee', {
            url: '/fee',
            templateUrl: 'maintenance/partial/fee/fee.html',
            controller: 'FeeMasterCtrl'
        })
        .state('maintenance.fee.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/fee/add-fee.html'
        })
        .state('maintenance.fee.upload', {
            url: '/upload',
            templateUrl: 'maintenance/partial/fee/upload.html'
        })
        .state('maintenance.fee.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/fee/edit-fee.html'

        });


    $stateProvider.state('maintenance.privilege', {
        url: '/privilege',
        templateUrl: 'maintenance/partial/privilege/privilege.html',
        controller: 'PrivilegeCtrl'
    })
    .state('maintenance.privilege.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/privilege/add-privilege.html'
        })
        .state('maintenance.privilege.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/privilege/edit-privilege.html'

        });
});
angular.module('maintenance').config(function($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
})
