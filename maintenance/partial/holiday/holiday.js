angular.module('maintenance').controller('HolidayCtrl', function($scope, $location, holidayService, utils) {

     $scope.view = 'list';
    $scope.holiday = {};

    function getHolidays() {
         console.log("Get function called from controller")
        holidayService.get().then(function(data) {
            $scope.HolidayInfo = data;
            $scope.HolidayInfo.forEach(function (holiday) {
                holiday.displayDate = utils.getDateString(holiday.holidayDate);
                holiday.holidayDate = new Date(holiday.holidayDate);
            })
        },function(err) {
            console.log(err);
        });
    }

    getHolidays();


    $scope.addHoliday = function() {
        $scope.holiday = {};
        $location.path('maintenance/holiday/add');
    }

    $scope.editHoliday = function(holiday) {
        console.log("holiday :" + holiday + " $scope.holiday :" + $scope.holiday);
        $scope.holiday = angular.extend({}, holiday);
        $location.path('maintenance/holiday/edit');
    }


    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            var holiday = angular.extend({}, $scope.holiday);
            delete holiday.displayDate; 
            holiday.holidayDate = holiday.holidayDate.toString();

            holidayService.save(holiday).then(function(data) {
                 if(data.message=="success"){
                $scope.holiday = {};
                getHolidays();
                $location.path('/maintenance/holiday')
             }else  $scope.addHolidayFormSubmitError=data.message;
            }, function(err) {
                $scope.addHolidayFormSubmitError = err.data.message;
                console.log('Error while adding holiday :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            var holiday = angular.extend({}, $scope.holiday);
            delete holiday.displayDate; 
            holiday.holidayDate = holiday.holidayDate.toString();

            holidayService.update(holiday).then(function(data) {
                $scope.holiday = {};
                getHolidays();
                $location.path('/maintenance/holiday')
            }, function(err) {
                console.log('Error while updating holiday :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        var holiday = angular.extend({}, $scope.currentHoliday);
        delete holiday.displayDate; 
        holiday.holidayDate = holiday.holidayDate.toString();

        holidayService.delete(holiday).then(function(data) {
          /*  for (var i = 0; i < $scope.HolidayInfo.length; i++) {
                if ($scope.HolidayInfo[i].timeStamp == $scope.currentHoliday.timeStamp) {
                    $scope.HolidayInfo.splice(i, 1);
                }
            }*/
            getHolidays();
            $location.path('/maintenance/holiday')
            $scope.currentHoliday = null;
            $('#deleteConfirmationModal').modal('hide');
        }, function(err) {
            console.log('Error while deleting record :', err);
        })
    }

    $scope.setCurrentHoliday = function(holiday) {
        $scope.currentHoliday = holiday;
    }
});
