
angular.module('maintenance').controller('PrivilegeCtrl',function($location ,$scope, PrivilegeService){

$scope.profile = {};
$scope.form={};
 $scope.search = function(event,form) {
        event.preventDefault();
        //console.log(form);

        if (form.$valid) {
        	
            PrivilegeService.search($scope.profile).then(function(data) {
            	console.log(data);
                $scope.ProfileInfo = data.result;
                console.log($scope.ProfileInfo);
                $location.path('/maintenance/privilege/add')
            }, function(err) {
               
                console.log('Error while adding band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

 $scope.add = function(data){
	$scope.profile = data;
	console.log("after search ",$scope.profile.firstName);
	$location.path('/maintenance/privilege/edit')
 };
 
 $scope.update = function( form) {
       
      	 if (true) {
        	console.log($scope.profile);
            PrivilegeService.edit($scope.profile).then(function(data) {
                $scope.profile = {};   
                  
                $location.path('/maintenance/privilege')
            }, function(err) {
               
                console.log('Error while updating profile :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
	};
    $scope.obj={};

    $scope.hi = function(data){
    console.log(data);
 };
});
