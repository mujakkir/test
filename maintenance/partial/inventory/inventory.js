angular.module('maintenance').controller('InventoryMasterCtrl', function($scope, $location, InventoryMasterService) {

    $scope.inventory = {};


    function getInventory() {
        InventoryMasterService.get().then(function(data) {
            $scope.InventoryInfo = data;
        }, function(err) {
            console.log(err);
        });
    }

    getInventory();

    $scope.addinventory = function() {
        $scope.inventory = {};
        $location.path('/maintenance/inventory/add');
    }

    $scope.editInventory = function(inventory) {
        $scope.inventory = inventory;
        $location.path('/maintenance/inventory/edit');
    }

    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            InventoryMasterService.save($scope.inventory).then(function(data) {
                $scope.inventory = {};
                getInventory();
                $location.path('/maintenance/inventory');
            }, function(err) {
                $scope.addFormSubmitError = err.data.message;
                console.log('Error while adding Inventory :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            InventoryMasterService.update($scope.inventory).then(function(data) {
                $scope.inventory = {};
                getInventory();
                $location.path('/maintenance/inventory')
            }, function(err) {
                console.log('Error while updating inventory :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        InventoryMasterService.delete($scope.currentInventory.timeStamp).then(function(data) {
            for (var i = 0; i < $scope.InventoryInfo.length; i++) {
                if ($scope.InventoryInfo[i].timeStamp == $scope.currentInventory.timeStamp) {
                    $scope.InventoryInfo.splice(i, 1);
                }
            }
            $scope.currentInventory = null;
            $('#confirmDelete').modal('hide');
        }, function(err) {
            console.log('Error while deleting record :', err);
        })
    }


    $scope.setCurrentInventory = function(Inventory) {
        $scope.currentInventory = Inventory;
    }
});
