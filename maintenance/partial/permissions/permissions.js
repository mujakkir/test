angular.module('maintenance').controller('PermissionsCtrl', function($scope, $location, permissionsService) {

    $scope.permissions = {};

    function getPermission() {
        permissionsService.get().then(function(data) {
            $scope.PermissionsInfo = data;
            console.log( $scope.PermissionsInfo);
        }, function(err) {
            console.log(err);
        });
    }

    getPermission();

    $scope.addPermissions = function() {
        $scope.permissions = {};
        $location.path('maintenance/permissions/add');
    }

    $scope.editPermissions = function(permissions) {
        console.log("permissions :" + permissions + " $scope.permissions :" + $scope.permissions);
        $scope.permissions = angular.extend({}, permissions);
        console.log($scope.permissions);
        $location.path('maintenance/permissions/edit');
    }

    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            permissionsService.save($scope.permissions).then(function(data) {
                $scope.permissions = {};
                getPermission();
                $location.path('/maintenance/permissions')
            }, function(err) {
                $scope.addFormSubmitError = err.data.message;
                console.log('Error while adding permissions :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

   $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            permissionsService.update($scope.permissions).then(function(data) {
                $scope.permissions = {};
                getPermission();
                $location.path('/maintenance/permissions')
            }, function(err) {
                console.log('Error while updating permissions :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        permissionsService.delete($scope.currentPermissions.timeStamp).then(function(data) {
            for (var i = 0; i < $scope.PermissionsInfo.length; i++) {
                if ($scope.PermissionsInfo[i].timeStamp == $scope.currentPermissions.timeStamp) {
                    $scope.PermissionsInfo.splice(i, 1);
                }
            }
            $scope.currentPermissions = null;
            $('#deleteConfirmationModal').modal('hide');
        }, function(err) {
            console.log('Error while deleting record :', err);
        })
    }
    
    $scope.setCurrentPermissions = function(permissions) {
        $scope.currentPermissions = permissions;
    }


});
