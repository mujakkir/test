 angular.module('maintenance').controller('HierarchyCtrl', function($scope, $location, HierarchyService) {

     $scope.hierarchy = {};



     function getHierarchys() {
         HierarchyService.get().then(function(data) {
             $scope.HierarchyInfo = data;
         }, function(err) {
             console.log(err);
         });
     }

     getHierarchys();

     $scope.addHierarchy = function() {
         $scope.hierarchy = {};
         $location.path('maintenance/hierarchy/add');
     }

     $scope.editHierarchy = function(hierarchy) {
         console.log("hierarchy :" + hierarchy + " $scope.hierarchy :" + $scope.hierarchy);
         $scope.hierarchy = angular.extend({}, hierarchy);
         console.log($scope.hierarchy);
         $location.path('maintenance/hierarchy/edit');
     }


     $scope.delete = function() {

         HierarchyService.delete($scope.currentHierarchy.timeStamp).then(function(data) {
             for (var i = 0; i < $scope.HierarchyInfo.length; i++) {
                 if ($scope.HierarchyInfo[i].timeStamp == $scope.currentHierarchy.timeStamp) {
                     $scope.HierarchyInfo.splice(i, 1);
                 }
             }
             $scope.currentHierarchy = null;
             $('#deleteConfirmationModal').modal('hide');
              }, function(err) {
            console.log('Error while deleting record :', err);
        });
     }


     $scope.setCurrentHierarchy = function(hierarchy) {
         $scope.currentHierarchy = hierarchy;
     }



 $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            HierarchyService.save($scope.hierarchy).then(function(data) {
                $scope.hierarchy = {};
                getHierarchys();
                $location.path('/maintenance/hierarchy')
            }, function(err) {
                $scope.addHierarchyFormSubmitError = err.data.message;
                console.log('Error while adding hierarchy :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

 $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            HierarchyService.update($scope.hierarchy).then(function(data) {
                $scope.hierarchy = {};
                getHierarchys();
                $location.path('/maintenance/hierarchy')
            }, function(err) {
                $scope.editHierarchyFormSubmitError = err.data.message;
                console.log('Error while updating hierarchy :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }


 });
