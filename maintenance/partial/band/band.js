angular.module('maintenance').controller('BandCtrl', function($scope, $location, BandService, $rootScope) {

    $scope.view = 'list';

    $scope.band = {};

    function getBands() {
       console.log("Get function called from controller")
        BandService.get().then(function(data) {
            $scope.BandInfo = data;
        }, function(err) {
            console.log(err);
        });
    }

    getBands();

    $scope.addband = function() {
        $scope.band = {};
        $scope.addBandFormSubmitError = null;
        $location.path('maintenance/band/add');
    }

    $scope.editband = function(ban) {
        $scope.band = angular.extend({}, ban);
        $location.path('maintenance/band/edit');
    }

    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            BandService.save($scope.band).then(function(data) {
                console.log(data);
                $scope.band = {};
                getBands();
                $location.path('/maintenance/band')
            }, function(err) {
                $scope.addBandFormSubmitError = err.data.message;
                console.log('Error while adding band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            BandService.update($scope.band).then(function(data) {
                $scope.band = {};
                getBands();
                $location.path('/maintenance/band')
            }, function(err) {
                $scope.editBandFormSubmitError = err.data.message;
                console.log('Error while updating band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {
        
        BandService.delete($scope.currentBand).then(function(data) {
            /*for (var i = 0; i < $scope.BandInfo.length; i++) {
                if ($scope.BandInfo[i].timeStamp == $scope.currentBand.timeStamp) {
                    $scope.BandInfo.splice(i, 1);
                }
            }*/
            getBands();
            $location.path('/maintenance/band')
            $scope.currentBand = null;
            $('#confirmDelete').modal('hide');
        }, function(err) {
            console.log('Error while deleting record :', err);
        })
    }

    $scope.setCurrentBand = function(Band) {
        $scope.currentBand = Band;
    }

});
