angular.module('maintenance').controller('FeeMasterCtrl', function($scope, $location, FeeMasterService) {

    $scope.fee = {};

    $scope.sendfile = {};
    $scope.myFile={};

    function getFee() {
        FeeMasterService.get().then(function(data) {
            $scope.FeeInfo = data;
        }, function(err) {
            console.log(err);
        });
    }

    getFee();

    $scope.addfee = function() {
        $scope.fee = {};
        $location.path('/maintenance/fee/add');
    }

    $scope.upload = function() {
        $scope.fee = {};
        $location.path('/maintenance/fee/upload');
    }


    $scope.download = function() {

        FeeMasterService.downloadFile(function(data) {

            $location.path('/maintenance/fee')
        })

    }

    /*    $scope.uploadFile = function() {

            console.log($scope.upload.file);

            $scope.sendfile.file=$scope.upload.file;

            console.log($scope.sendfile);


            FeeMasterService.upload($scope.sendfile, function(data) {
                    $scope.upload = {};
                    getFee();
                    $location.path('/maintenance/fee')
                })
        }*/


    $scope.uploadFile = function() {

        var file = $scope.myFile;
        console.log('file is ' + $scope.myFile);
        console.log(file);

        FeeMasterService.uploadFileToUrl(file);
    };





    $scope.editFee = function(fe) {
        $scope.fee = angular.extend({}, fe);
        $location.path('/maintenance/fee/edit');
    }


    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            var a = $scope.fee.miscellaneousFee;
            var b = $scope.fee.sportsFee;
            var c = $scope.fee.transportationFee;
            var d = $scope.fee.tuitionFee;

            $scope.fee.totalFee = a + b + c + d;



            console.log($scope.fee.totalFee);
            FeeMasterService.save($scope.fee).then(function(data) {
                $scope.fee = {};
                getFee();
                $location.path('/maintenance/fee')
            }, function(err) {
                $scope.addFormSubmitError = err.data.message;
                console.log('Error while adding band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }


    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            FeeMasterService.update($scope.fee).then(function(data) {
                $scope.fee = {};
                getFee();
                $location.path('/maintenance/fee')
            }, function(err) {
                console.log('Error while updating fee :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        FeeMasterService.delete($scope.currentFee.timeStamp).then(function(data) {
            for (var i = 0; i < $scope.FeeInfo.length; i++) {
                if ($scope.FeeInfo[i].timeStamp == $scope.currentFee.timeStamp) {
                    $scope.FeeInfo.splice(i, 1);
                }
            }
            $scope.currentFee = null;
            $('#confirmDelete').modal('hide');
        }, function(err) {
            console.log('Error while deleting record :', err);
        })
    }

    $scope.setCurrentFee = function(Fee) {
        $scope.currentFee = Fee;
    }
});
