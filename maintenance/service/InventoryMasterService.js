angular.module('maintenance').service('InventoryMasterService', function($resource, config) {
    var inventory = $resource(config.baseUrl +'/inventory/:ts/', {ts: '@timeStamp'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function () {
        return inventory.query().$promise;
    }

    this.save = function (data) {
        return inventory.save(data).$promise;
    }

    this.update = function (data) {
        return inventory.update({ts: null}, data).$promise;
    }

    this.delete = function (timestamp) {
        return inventory.delete({ts: timestamp.toString()}).$promise;
    }
});
    