angular.module('maintenance').service('HierarchyService', function($resource, config) {
    var hierarchy = $resource(config.baseUrl +'/hierarchy/:ts/', {ts: '@timeStamp'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function () {
        return hierarchy.query().$promise;
    }

    this.save = function (data) {
        return hierarchy.save(data).$promise;
    }

    this.update = function (data) {
        return hierarchy.update({ts: null}, data).$promise;
    }

    this.delete = function (timestamp) {
        return hierarchy.delete({ts: timestamp.toString()}).$promise;
    }
});