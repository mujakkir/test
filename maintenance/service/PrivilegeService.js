angular.module('maintenance').service('PrivilegeService',function($resource, config) {

	
	var privilege = $resource(config.baseUrl +'/privilege', {} ,{
	     'update': { method: 'PUT' }	
	});

	 this.search = function (data) {
        return privilege.save(data).$promise;
    }

    this.edit = function (data) {
        return privilege.update(data).$promise;
    }
});