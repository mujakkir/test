angular.module('maintenance').service('permissionsService', function($resource, config) {
    var permission = $resource(config.baseUrl +'/permission/:ts/', {ts: '@timeStamp'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function () {
        return permission.query().$promise;
    }

    this.save = function (data) {
        return permission.save(data).$promise;
    }

    this.update = function (data) {
        return permission.update({ts:null},data).$promise;
    }

    this.delete = function (timestamp) {
        return permission.delete({ts: timestamp.toString()}).$promise;
    }
});
