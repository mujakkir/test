
angular.module('maintenance').service('BandService', function($resource, config, $rootScope, $cookies) {
    

    var band = $resource(config.baseUrl +'/band/:task/', {task: '@task'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function () {
        console.log("get function");   
        console.log($cookies.get("role")) 
        return band.query().$promise;
    }

    this.save = function (data) {
        console.log(data);
        return band.save( data).$promise;
    }

    this.update = function (data) {
        console.log(data);
        return band.update({ task: "edit"}, data).$promise;
        console.log("after update");
    }

    this.delete = function (data) {
        return band.update({ task: "delete"}, data).$promise;
    }
});
	
