angular.module('maintenance').service('holidayService', function($resource, config) {
    var holiday = $resource(config.baseUrl +'/holiday/:ts/', {ts: '@task'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function () {
        return holiday.query().$promise;
    }

    this.save = function (data) {
        return holiday.save(data).$promise;
    }

    this.update = function (data) {
         console.log(data);
        return holiday.update({ts: "edit"}, data).$promise;
        console.log("after update");
    }

    this.delete = function (data) {
        console.log(data);
        return holiday.update({ts: "delete"},data).$promise;
        console.log("after update");
    }
});


/*this.update = function (data) {
        console.log(data);
        return band.update({ task: "edit"}, data).$promise;
        console.log("after update");
    }

    this.delete = function (data) {
        return band.update({ task: "delete"}, data).$promise;
    }*/