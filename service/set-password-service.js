angular.module('gsmart').service('SetPasswordService',function($resource, config) {

	var setPassword = $resource(config.baseUrl + '/password', {}, {});

	this.save = function (password) {
		return setPassword.save(password).$promise;
	}
});