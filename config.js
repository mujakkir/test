angular.module('appConfig', [])
.constant('config', {
	'webService' : {
		'scheme' : 'http',
		'host': 'localhost',
		'port' : '8080',
		'app' : 'gsmart'
	},
	baseUrl: 'http://localhost:8080/gsmart'
})