angular.module('performanceAppraisal', ['ui.bootstrap','ui.utils','ui.router']);

angular.module('performanceAppraisal').config(function($stateProvider) {

     $stateProvider.state('performance', {
        url: '/performance',
        parent:'root',
        templateUrl: 'performance_appraisal/partial/performance-appraisal.html',
        controller: 'PerformanceAppraisalCtrl'
        })
        

});

