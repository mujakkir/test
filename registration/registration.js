angular.module('registration', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngResource']);

angular.module('registration').config(function($stateProvider) {

    $stateProvider.state('registration', {
            url: '/registration',
            parent: 'app',
            templateUrl: 'registration/home.html'
        })
        .state('registration.employee', {
            url: '/employee',
            templateUrl: 'registration/partial/employee/home.html',
            controller: 'EmployeeCtrl'
        })
        .state('registration.employee.add', {
            url: '/add',
            templateUrl: 'registration/partial/employee/registration.html'
        })
        .state('registration.employee.edit', {
            url: '/edit',
            templateUrl: 'registration/partial/employee/edit.html'
        })
        

        .state('registration.student', {
            url: '/student',
            templateUrl: 'registration/partial/student/home.html',
            controller: 'StudentCtrl'
        }).state('registration.student.add', {
            url: '/add',
            templateUrl: 'registration/partial/student/registration.html'
        }).state('registration.student.edit', {
            url: '/edit',
            templateUrl: 'registration/partial/student/edit.html'
        })

});
