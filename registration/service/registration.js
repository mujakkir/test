angular.module('registration').service('registration', function($resource, $cookies, config) {


    var reg = $resource(config.baseUrl + '/registration', {}, {

    });

    this.get = function() {
        return reg.get().$promise;
    }


    var getStudentProfilesReg = $resource(config.baseUrl + '/registration/student', {}, {});

    this.getStudentProfiles = function() {
        return getStudentProfilesReg.get().$promise;
    }


    var getEmployeeProfilesReg = $resource(config.baseUrl + '/registration/employee', {}, {});

    this.getEmployeeProfiles = function() {
        return getEmployeeProfilesReg.get().$promise;
    }


    this.addProfile = $resource(config.baseUrl + '/registration/addProfile/' + $cookies.get('userId'), {}, {
        add: { method: 'POST' }

    })

    this.editProfile = $resource(config.baseUrl + '/registration/updateProfile/' + $cookies.get('userId'), {}, {
        update: { method: 'POST' }

    })

    this.search = $resource(config.baseUrl + '/registration/searchRep/', {}, {
        add: { method: 'POST' }
    })
});
