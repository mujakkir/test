angular.module('registration').controller('EmployeeCtrl', function($scope, $location, registration, BandService, HierarchyService) {

    $scope.profile = {};
    $scope.currentInstitution = {};
    $scope.currentSchool = {};
    $scope.currentBand = {};
    $scope.currentRole = {};
    $scope.currentDesignation = {};

    $scope.search = {};
    $scope.loading = true;


    
    registration.getEmployeeProfiles().then(function(data) {
        $scope.profileInfo = data;
        $scope.loading = false;
    },function(err) {
        console.log(err);
    });

    /*registration.get().then(function(data) {
        $scope.profileInfo = data;
        $scope.loading = false;
    }, function(err) {
        console.log(err);
    });
*/
    BandService.get().then(function(data) {
        $scope.BandInfo = data;
    }, function(err) {
        console.log(err);
    });


    HierarchyService.get().then(function(data) {
        $scope.HierarchyInfo = data;
    }, function(err) {
        console.log(err);
    });

    $scope.addProfile = function(profile) {
        $scope.profile = {};
        $location.path('registration/employee/add');
    }

    $scope.editProfile = function(profile) {
        $scope.profile = profile;
        $scope.currentBand = _.find($scope.BandInfo, { bandId: $scope.profile.band });
        $scope.currentInstitution = {
            institution: $scope.profile.institution
        };
        $scope.currentSchool = {
            school: $scope.profile.school
        };
        $scope.currentDesignation = {
            designation: $scope.profile.designation
        };
        $scope.currentRole = {
            role: $scope.profile.role
        };
        $scope.search.name = $scope.profile.reportingManagerId;
        $scope.profile.dob = new Date($scope.profile.dob);
        $scope.profile.joiningDate = new Date($scope.profile.joiningDate);
        $scope.profile.passportIssueDate = new Date($scope.profile.passportIssueDate);
        $scope.profile.passportExpiryDate = new Date($scope.profile.passportExpiryDate);

        $location.path('/registration/employee/edit');
    }

    $scope.searchs = function() {

        console.log('search called');
        $scope.search.band=$scope.currentBand.bandId;
        $scope.search.school=$scope.currentSchool.school;
        console.log($scope.search);
        registration.search.add($scope.search, function(data) {
            console.log(data);
            $scope.repInfo = data;
        });
    }


    $scope.editrep = function(rep) {
        $scope.search.name = rep.smartId
        $scope.profile.reportingManagerName = rep.firstName
        $scope.profile.counterSigningManagerId = rep.reportingManagerId
        $scope.profile.counterSigningManagerName = rep.reportingManagerName
        $('#myModal').modal('hide')
    }

    $scope.save = function(event, registrationForm) {
        if (registrationForm.$valid) {
            $scope.profile.reportingManagerId = $scope.search.name;
            $scope.profile.institution = $scope.currentInstitution.institution;
            $scope.profile.school = $scope.currentSchool.school;
            $scope.profile.role = $scope.currentRole.role;
            $scope.profile.band = $scope.currentBand.bandId;
            $scope.profile.designation = $scope.currentDesignation.designation;
           
/*
           if ($scope.profile.image.base64;) {
                $scope.profile.image = 
                console.log($scope.profile.image);
           }*/
         

            registration.addProfile.add($scope.profile, function(data) {
                $scope.profiles = data;
                $location.path('/registration')
            });
        } else {

        }
    }

    $scope.update = function(event, registrationForm) {

        if (registrationForm.$valid) {

            console.log('UPDATE CALLED');
            console.log($scope.currentInstitution.institution);
            $scope.profile.reportingManagerId = $scope.search.name;
            $scope.profile.institution = $scope.currentInstitution.institution;
            $scope.profile.school = $scope.currentSchool.school;
            $scope.profile.role = $scope.currentRole.role;
            $scope.profile.band = $scope.currentBand.bandId;
            $scope.profile.designation = $scope.currentDesignation.designation;

            registration.editProfile.update($scope.profile, function(data) {
                $scope.profiles = data;
                $location.path('/registration')
            });
        } else {
            console.log('editProfile err'+registrationForm);

        }

    }


});
