angular.module('registration').controller('StudentCtrl', function($scope, registration, $location, BandService, HierarchyService) {

    $scope.profile = {};
    $scope.currentInstitution = {};
    $scope.currentSchool = {};
    $scope.currentBand = {};
    $scope.currentRole = {};
    $scope.currentDesignation = {};

    $scope.currentClassName = {};
    $scope.currentSection = {};
    $scope.search = {};


    registration.getStudentProfiles().then(function(data) {
        $scope.profileInfo = data;
        $scope.loading = false;
    },function(err) {
        console.log(err);
    });


    BandService.get().then(function(data) {
        $scope.BandInfo = data;
    }, function(err) {
        console.log(err);
    });


    HierarchyService.get().then(function(data) {
        $scope.HierarchyInfo = data;
    }, function(err) {
        console.log(err);
    });

    $scope.addProfile = function(Profile) {
        $scope.profile = {};
        $location.path('registration/student/add');
    }



$scope.editProfile = function(profile) {
    console.log('edit called')
        $scope.profile = profile;
        $scope.currentBand = _.find($scope.BandInfo, { bandId: $scope.profile.band });
        $scope.currentInstitution = {
            institution: $scope.profile.institution
        };
        $scope.currentSchool = {
            school: $scope.profile.school
        };
        $scope.currentDesignation = {
            designation: $scope.profile.designation
        };
        $scope.currentRole = {
            role: $scope.profile.role
        };

        $scope.currentClassName = {
            standard: $scope.profile.standard
        };
        $scope.currentSection = {
            section: $scope.profile.section
        };

        console.log($scope.profile.reportingManagerId)
        $scope.search.name = $scope.profile.reportingManagerId;
        $scope.profile.dob = new Date($scope.profile.dob);
        $scope.profile.previousYear = new Date($scope.profile.previousYear);
       
        $location.path('/registration/student/edit');

    }

/*
    $scope.searchs = function() {
        console.log('search called');
        console.log($scope.profile.band);
        console.log($scope.search);

        registration.searching($scope.profile.band).repSearch($scope.search, function(data) {
            $scope.repInfo = data;
            console.log(data);
        });
    }
*/


    $scope.searchs = function() {

        console.log('search called');
        $scope.search.band=$scope.profile.band;
        $scope.search.school=$scope.currentSchool.school;
        console.log($scope.search);
        registration.search.add($scope.search, function(data) {
            console.log(data);
            $scope.repInfo = data;
        });
    }

    $scope.editrep = function(rep) {
        $scope.search.name = rep.smartId;
        $scope.profile.reportingManagerName = rep.firstName
        $('#myModals').modal('hide')
    }


    $scope.save = function(event, registrationForm) {
        
        console.log(registrationForm)
        if (registrationForm.$valid) {

            $scope.profile.institution = $scope.currentInstitution.institution;
            $scope.profile.school = $scope.currentSchool.school;
            $scope.profile.standard = $scope.currentClassName.standard;
            $scope.profile.section = $scope.currentSection.section;
            $scope.profile.role = $scope.currentRole.role;
            $scope.profile.designation = $scope.currentDesignation.designation;
            $scope.profile.reportingManagerId = $scope.search.name;
            console.log($scope.profile);
            
            registration.addProfile.add($scope.profile, function(data) {
                $scope.profiles = data;
                $location.path('/registration')
            });
        } else {
            console.log('error')
        }
    }



    $scope.update = function(event, registrationForm) {
        console.log('call')

        if (registrationForm.$valid) {  
            console.log('UPDATE CALLED');
            console.log($scope.currentInstitution.institution);
            $scope.profile.reportingManagerId = $scope.search.name;
            $scope.profile.institution = $scope.currentInstitution.institution;
            $scope.profile.school = $scope.currentSchool.school;
            $scope.profile.role = $scope.currentRole.role;
            $scope.profile.band = $scope.currentBand.bandId;
            $scope.profile.designation = $scope.currentDesignation.designation;
            $scope.profile.section= $scope.currentSection.section;
            $scope.profile.standard= $scope.currentClassName.standard;

            registration.editProfile.update($scope.profile, function(data) {
                $location.path('/registration')
            });
        } else {
             console.log('editProfile err'+registrationForm);

        }

    }

});
