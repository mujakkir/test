angular.module('gsmart', ['appConfig', 'ui.bootstrap', 'ui.utils', 'ui.router', 'ngResource', 'ngCookies', 'common', 'maintenance', 'registration', 'organizationStructure', 'inventory', 'contactTeacher', 'fee', 'reportCard', 'notice', 'leave', 'performanceAppraisal', 'general', 'utils','naif.base64','ngFileUpload']);

angular.module('gsmart').config(function($stateProvider, $urlRouterProvider) {

    /* Add New States Above */
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('app', {
        url: '',
        abstract: true,
        templateUrl: 'partial/home/home.html',  
        controller: 'HomeCtrl'
    })
    .state('main', {
        url:'/home',
        parent: 'app',
        templateUrl: 'partial/dashboard/dashboard.html',
        controller: 'DashboardCtrl'
    });
    $stateProvider.state('setPassword', {
        url: '/setPassword/:smartId',
        templateUrl: 'partial/set-password/set-password.html',
        controller: 'SetPasswordCtrl'
    });  
    $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'partial/login/login.html',
        controller: 'LoginCtrl'
    })
    .state('profile', {
            url: '/profile',
            parent: 'app',
            templateUrl: 'partial/profile/profile.html',
            controller: 'ProfileCtrl',
            resolve: {
                template: function ($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function (user) {             
                        var template = null;
                        console.log(user);
                        if(user.profile.role.toLowerCase() === 'student') {
                            console.log('student'+user.profile.role.toLowerCase() )
                            template = 'partial/profile/studentProfile.html';
                        } else {
                             console.log(' ? ' +user.profile.role.toLowerCase() )
                            template = 'partial/profile/employeeProfile.html';
                        }
                        console.log('pageTobeLoaded :', template);
                        defer.resolve(template);
                    });
                    return defer.promise;
                }
            }
        });



}).run(function($rootScope, $location, $state, User) {
    $rootScope.$on('$stateChangeStart', function(e, toState  , toParams, fromState, fromParams) {

        if(toState.name === 'login'){
           return; // no need to redirect 
        } else if (toState.name === 'home') {
            return;
        }

        // now, redirect only not authenticated

        User.getUserInfo(function (userInfo) {
            console.log(userInfo);
             if(!userInfo) {
                e.preventDefault(); // stop current execution
                $state.go('login'); // go to login
             }
        });     
    });

    $rootScope.$on('loginChanged', function () {
        if (!User.userInfo) {
            $state.go('login');
        }
    });
})

angular.module('gsmart').run(function($rootScope) {
    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
});
