angular.module('organizationStructure').controller('OrgstructureCtrl', function($scope, User, $stateParams, orgStructure, $location) {
    var organizationTree = null;
    $scope.empId = $stateParams.empId;
    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.search = {};
    
    var parentId = $stateParams.empId ? $stateParams.empId : User.id;

    orgStructure.get(parentId).then(function(data) {
        console.log(parentId);
        $scope.currentEmp = data.selfProfile;
        $scope.people = data.childList;
        console.log($scope.currentEmp );
    }, function(err) {
        console.log('Error while fetching orgStructure :', err);
    });

    $scope.showOrganizationTree = function(empId) {
        $location.path('/orgstructure/'+ empId);
    }

    $scope.goToSearch = function() {
        orgStructure.save(User.id, $scope.search).then(function(data) {
            $scope.searchResults = data.childList;
            $location.path('/orgstructure/search');
            $location.search($scope.search);
            search();
        }, function(err) {
            $scope.orgStructureError = err.data.message;
        });
    }

    function search () {
        orgStructure.save(User.id, $scope.search).then(function(data) {
            $scope.searchResults = data.childList;
        }, function(err) {
            $scope.orgStructureError = err.data.message;
        });
    }

    $scope.search = $location.search();

    if($scope.search.name) {
        search();
    } else {
        $location.search({});
    }
});
