angular.module('organizationStructure').controller('EmployeeDetailsCtrl', function($scope, User, $stateParams,$location, orgStructure) {

    $scope.empDetails = {};

    console.log( $stateParams.empId);
	var childId = $stateParams.empId ? $stateParams.empId : User.id;

    if (childId != User.id) {
        console.log(childId);
        orgStructure.getEmployeeDetails(childId, function(empDetails) {

            if (empDetails) {
                $scope.empDetails = empDetails;
            }

        })
    } else {
    	console.log(User.userInfo.profile);
        $scope.empDetails = User.userInfo.profile;
        console.log($scope.empDetails);
    }




});
