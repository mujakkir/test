angular.module('organizationStructure', ['ui.bootstrap','ui.utils','ui.router']);

angular.module('organizationStructure').config(function($stateProvider) {

   $stateProvider.state('organizationStructure', {
        url: '/orgstructure',
        parent: 'app',
        templateUrl: 'organization_structure/partial/orgStructure.html',
        controller: 'OrgstructureCtrl'    
    })
   .state('organizationStructure.search', {
        url: '/search',
        templateUrl: 'organization_structure/partial/search.html' 
    })
    .state('organizationStructure.employee', {
        url: '/:empId',
        templateUrl: 'organization_structure/partial/orgStructure.html',
        controller: 'OrgstructureCtrl'    
    })
    .state('employeeDetails', {
        url: '/employeeDetails/:empId',
        parent: 'app',
        templateUrl: 'organization_structure/partial/employeeDetails/employeeDetails.html',
        controller: 'EmployeeDetailsCtrl' 
    });
});

