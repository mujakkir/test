angular.module('organizationStructure').service('orgStructure',function($resource,config, $q) {

	var organizationTree = null;

    var organizationStructure = $resource(config.baseUrl + '/org/:id', {id: '@id'}, {});

    this.get = function (id) {
        var defer = $q.defer();
        var params = {
            id: id
        };
        
        organizationStructure.get(params).$promise.then(function (data) {
            organizationTree = data;
            defer.resolve(data);
        }, function (err) {
            defer.reject(err);
        });

        return defer.promise;
    }

	this.getEmployeeDetails = function (id, callback) {
        var empDetails = null;
        console.log(organizationTree);
        
    	if (organizationTree && organizationTree.childList.length > 0) {
            for(var i=0; i<organizationTree.childList.length; i++) {
                if (organizationTree.childList[i].smartId.toString() == id) {
                    empDetails = organizationTree.childList[i];
                    break;
                }
            }
            callback(empDetails);
        }
        else {
            //make the web service call to fetch employee details using id
        }
    }


    var empSearch = $resource(config.baseUrl +'/org/search/:id', {id: '@id'}, {
         'update': { method: 'PUT' }
    });
 
    this.save = function (id,data) {
        return empSearch.save({id: id}, data).$promise;
    }
});