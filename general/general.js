angular.module('general', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('general').config(function($stateProvider) {

    $stateProvider.state('home', {
        url: '/',
        templateUrl: 'general/partial/home/home.html',
        controller: 'Home1Ctrl'
    });
    
});

