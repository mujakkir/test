angular.module('leave').service('LeaveService',function($resource, config) {
 var leave = $resource(config.baseUrl +'/leave/:ts/', {ts: '@task'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function () {
        return leave.query().$promise;
    }

    this.save = function (data) {
        return leave.save(data).$promise;
    }

    this.update = function (data) {
        return leave.update({ts:"edit"}, data).$promise;
    }

    this.delete = function (data) {
        return leave.update({ts:"delete"},data).$promise;
    }
});