angular.module('leave', ['appConfig','ui.bootstrap','ui.router', 'ui.utils', 'ui.router', 'utils']);

angular.module('leave').config(function($stateProvider) {

    $stateProvider.state('leave', {
        url: '/leave',
        parent:'app',
        templateUrl: 'leave/partial/leave.html',
        controller: 'LeaveCtrl'
    })

    /* Add New States Above */
    .state('leave.add', {
            url: '/add',
            templateUrl: 'leave/partial/add-leave.html'
        })
     .state('leave.edit', {
            url: '/edit',
            templateUrl: 'leave/partial/edit-leave.html'
        })
  


});

