angular.module('leave').controller('LeaveCtrl',function($scope,$location,LeaveService,utils){
    $scope.leave = {};
    $scope.view = 'list';



     function getLeaves() {
         LeaveService.get().then(function(data) {
             $scope.LeaveInfo = data;
             $scope.LeaveInfo.forEach(function (leave) {
                leave.displaystartDate = utils.getDateString(leave.startDate);
                leave.displayendDate = utils.getDateString(leave.endDate);
                leave.startDate = new Date(leave.startDate);
                leave.endDate = new Date(leave.endDate); 
            })
         }, function(err) {
             console.log(err);
         });
     }

     getLeaves();

     $scope.addLeave = function() {
         $scope.leave = {};
         $location.path('leave/add');
     }

     $scope.editLeave = function(leave) {
         console.log("leave :" + leave + " $scope.leave :" + $scope.leave);
         $scope.leave = angular.extend({}, leave);
         console.log($scope.leave);
         $location.path('leave/edit');
     }


     $scope.delete = function() {
         var leave = angular.extend({}, $scope.currentLeave);
             delete leave.displayendDate;
             delete leave.displaystartDate;
              leave.startDate = leave.startDate.toString();
              leave.endDate = leave.endDate.toString();

         LeaveService.delete(leave).then(function(data) {
             /*for (var i = 0; i < $scope.LeaveInfo.length; i++) {
                 if ($scope.LeaveInfo[i] == $scope.currentLeave) {
                     $scope.LeaveInfo.splice(i, 1);
                 }
             }*/
             getLeaves();
              $location.path('/leave')
             $scope.currentLeave = null;
             $('#deleteConfirmationModal').modal('hide');
              }, function(err) {
            console.log('Error while deleting record :', err);
        });
     }


     $scope.setCurrentLeave = function(leave) {
         $scope.currentLeave = leave;
     }



 $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
             var leave = angular.extend({}, $scope.leave);
             delete leave.displayendDate;
             delete leave.displaystartDate;
              leave.startDate = utils.getDateString(leave.startDate);
              leave.endDate = utils.getDateString(leave.endDate);

            LeaveService.save($scope.leave).then(function(data) {
                $scope.leave = {};
                getLeaves();
                $location.path('/leave')
            }, function(err) {
                $scope.addLeaveFormSubmitError = err.data.message;
                console.log('Error while adding leave :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

 $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            var leave = angular.extend({}, $scope.leave);
             delete leave.displayendDate;
             delete leave.displaystartDate;
              leave.startDate = leave.startDate.toString();
              leave.endDate = leave.endDate.toString();

            LeaveService.update(leave).then(function(data) {
                $scope.leave = {};
                getLeaves();
                $location.path('/leave')
            }, function(err) {
                $scope.editLeaveFormSubmitError = err.data.message;
                console.log('Error while updating leave :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }


});