angular.module('contactTeacher').service('ContactTeacherService',function($resource,$cookies) {

	this.postMsg = $resource('/gsmart/contact/studentToTeacher', {}, {
        add: { method: 'POST' }
    });

   this.getMsgs = $resource('/gsmart/contact/msgListForTeacher', {}, {
        view: { method: 'POST' }
    }); 

	this.chat = $resource('/gsmart/contact/teacherView', {}, {
        view2: { method: 'POST' }
    }); 

    this.tMsg = $resource('/gsmart/contact/teacherToStudent', {}, {
        view3: { method: 'POST' }
    }); 

    this.sMsg = $resource('/gsmart/contact/studentView', {}, {
        view4: { method: 'POST' }
    });

    this.organisationTree = function (id) {
        return $resource('/gsmart/contactSearch/info/' + id, {}, {
            get: { method: 'POST', headers: { 'Content-Type': 'application/json' } }
        })
    } 
});