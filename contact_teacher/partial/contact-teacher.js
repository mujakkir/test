angular.module('contactTeacher').controller('ContactTeacherCtrl',function($scope,User,$stateParams,$location,$cookies,ContactTeacherService){

$scope.MessageDetails={};

//Contact Strructure
    var organizationTree = null;
    $scope.empId = $stateParams.empId;
    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.msgDetails= {};


 $scope.MessageDetails.reportingManagerId=$cookies.get('userId');
  ContactTeacherService.getMsgs.view($scope.MessageDetails, function(data) {
        $scope.msgData = data;
        console.log( $scope.msgData );
    });


 $scope.save = function(event, form) {
          event.preventDefault();
         if (form.$valid){
        $scope.MessageDetails.smartId=$cookies.get('userId');
        $scope.MessageDetails.reportingManagerId=User.userInfo.empDetails.reportingManagerId;
        $scope.MessageDetails.studentName=User.userInfo.empDetails.firstName;
        ContactTeacherService.postMsg.add($scope.MessageDetails, function(data) {
            if (data.result === 'MSG Posted') {
              alert("Message Posted")
              if ($scope.MessageDetails.message !== "") {
                  $scope.MessageDetails.message = "";
                   $scope.addMessageForm.$setPristine();  
              }
                {{data.result}}
            }
        });
      }
       else {
             angular.forEach(form.$error.required, function(field) {
             field.$setDirty();
          });
    }
  }



     $scope.chatMsg = function(info) {
        ContactTeacherService.chat.view2(info, function(data) {
        $scope.chatData=data;
        $location.path('/messages/chat');
    });
     }

//Teacher Message to Student-Dynamic
     $scope.teachMsg = function(event,form,MessageDetails) {
        event.preventDefault();
        if (form.$valid){
          $scope.MessageDetails.smartId=$scope.chatData.result[0].smartId;
         ContactTeacherService.tMsg.view3($scope.MessageDetails, function(data) {
          alert("Message Posted")
          if ($scope.MessageDetails.message !== "") {
                  $scope.MessageDetails.message = " ";
                  //$scope.teacherMesForm.$setPristine();  
              }
    });
     }
     else {
             angular.forEach(form.$error.required, function(field) {
             field.$setDirty();
          });
     }
      }





     $scope.studentMsg = function() {
         $scope.MessageDetails.smartId=$cookies.get('userId');
        $scope.MessageDetails.reportingManagerId=User.userInfo.empDetails.reportingManagerId;
        ContactTeacherService.sMsg.view4($scope.MessageDetails, function(datas) {
        $scope.sData = datas;
    });
     }


      //Direct Teacher to Student

       $scope.studentlist = function() {
        $scope.studentlist = {};
        $location.path('/messages/studentlist');
    }

     $scope.tMsg = function(p) {
         $scope.MessageDetails.smartId=p.smartId;
        $scope.MessageDetails.reportingManagerId=p.reportingManagerId;
        ContactTeacherService.chat.view2($scope.MessageDetails, function(data) {
        $scope.tData=data;
        $location.path('/messages/send');
    });
     }

      $scope.teachinitialMsg = function(event,form,MessageDetails) {
        event.preventDefault();
        if (form.$valid){
          console.log('start')
          console.log($scope.MessageDetails);
          console.log('start')
         ContactTeacherService.tMsg.view3($scope.MessageDetails, function(data) {
          alert("Message Posted")
          if ($scope.MessageDetails.message !== "") {
              $scope.MessageDetails.message = " ";
              }
    });
     }
     else {
             angular.forEach(form.$error.required, function(field) {
             field.$setDirty();
          });
     }
      }

//Contact Structure

 function loadChildren() {
        organizationTree.get({}, function(data) {
            $scope.currentEmp = data.result.selfProfile;
            $scope.people = data.result.childList;
            $scope.msgDetails = data.result.childMessages;
        });
    }

    if ($stateParams.empId) {
        organizationTree = ContactTeacherService.organisationTree($stateParams.empId);
        loadChildren();
    }
    else {
        organizationTree = ContactTeacherService.organisationTree(User.id);
        loadChildren();
    }

    $scope.showOrganizationTree = function(empId) {
        console.log(empId);
        $location.path('messages/contactstructure/' + empId);
    }



});
